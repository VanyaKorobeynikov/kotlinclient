import React, { Component } from 'react';
import './Photos.css';

class Photos extends Component{
  render() {
    const photos = [
      {
        path: './images/gallery/gallery1.jpg',
      },
      {
        path: './images/gallery/tab1.png',
      },
      {
        path: './images/gallery/gallery1.jpg',
      },
      {
        path: './images/gallery/gallery1.jpg',
      },
      {
        path: './images/gallery/gallery1.jpg',
      },
      {
        path: './images/gallery/tab1.png',
      }
    ];
    const photoList = photos.map((item,index) => {
      return (
        <div className="photo" key={index}>
          <img src={item.path} alt={index}/>
        </div>
      )
    }) 
    return (
      <section id="blog" className="container">
        <div className="container">
          <div className="center">
             <h2>Фотогалерея</h2>
          </div>
          <div className="row">
            <div className="col-sm-6 col-sm-offset-3 year-row">
              <a href="#" className="year-button">2016</a>
              <a href="#" className="year-button">2016</a>
              <a href="#" className="year-button">2016</a>
            </div>
          </div>
          <div className="row">
            <div className="flex-container">
            {photoList}
            </div>
          </div>
        </div>
      </section>
      
    )
  }
}

export default Photos;