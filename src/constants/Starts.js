export const GET_STARTS_REQUEST  = 'GET_STARTS_REQUEST';
export const GET_STARTS_SUCCESS  = 'GET_STARTS_SUCCESS';
export const GET_STARTS_FAIL     = 'GET_STARTS_FAIL';
export const ADD_START_REQUEST  = 'ADD_START_REQUEST';
export const ADD_START_SUCCESS  = 'ADD_START_SUCCESS';
export const ADD_START_FAIL     = 'ADD_START_FAIL';
export const GET_START_REQUEST  = 'GET_START_REQUEST';
export const GET_START_SUCCESS  = 'GET_START_SUCCESS';
export const GET_START_FAIL     = 'GET_START_FAIL';
export const DELETE_START_REQUEST  = 'DELETE_START_REQUEST';
export const DELETE_START_SUCCESS  = 'DELETE_START_SUCCESS';
export const DELETE_START_FAIL     = 'DELETE_START_FAIL';
export const EDIT_START_SUCCESS  = 'EDIT_START_SUCCESS';