import React, { Component } from 'react';
import { Link } from 'react-router';
import './Admin.css';

class Admin extends Component {
  render() {
    return (
      <div>
        <div className="header" id="header">
        <h1 className="site_title" id="site_title">Kotlin</h1>
        <ul className="header-item tabs" id="tabs">
          <li id="admin_users"><Link to="/admin/admin_users">Admin Users</Link></li>
          <li id="comments"><Link to="/admin/comments">Comments</Link></li>
          <li id="finishers"><Link  activeClassName="active" to="/admin/finishers">Finishers</Link></li>
          <li id="images"><Link to="/admin/images">Images</Link></li>
          <li id="pages"><Link to="/admin/pages">Pages</Link></li>
          <li id="reports"><Link to="/admin/reports">Reports</Link></li>
          <li id="requests"><Link to="/admin/requests">Requests</Link></li>
          <li id="starts"><Link to="/admin/starts">Starts</Link></li>
        </ul>
        <ul className="header-item tabs" id="utility_nav">
          <li id="logout"><Link to="logout">Logoutss</Link></li>
        </ul>
      	</div>
        {this.props.children}
      </div>
    )
  }
}

export default Admin;