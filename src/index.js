import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import cookie from 'react-cookie'
import App from './App'
import Admin from './admin/Admin'
import Finishers from './components/Finishers'
import AddFinisher from './admin/components/AddFinisher'
import EditFinisher from './admin/components/EditFinisher'
import EditStart from './admin/components/EditStart'
import AdminFinishers from './admin/components/AdminFinishers'
import AddStart from './admin/components/AddStart'
import AdminStarts from './admin/components/AdminStarts'
import AddPhoto from './admin/components/AddPhoto'
import AddPartner from './admin/components/AddPartner'
import AdminPhoto from './admin/components/AdminPhoto'
import AdminPartner from './admin/components/AdminPartner'
import AdminRequest from './admin/components/AdminRequest'
import Photos from './components/Photos'
import SliderMain from './components/Slider'
import Reports from './components/Reports'
import Report from './components/Report'
import EditPhoto from './admin/components/EditPhoto'
import Login from './admin/components/Login'
import Logout from './admin/components/Logout'
import Register from './admin/components/Register'
import Request from './components/Request'
import About from './components/About'
import Rules from './components/Rules'
import History from './components/History'
import Partners from './components/Partners'
import Authors from './components/Authors'
import Culture from './components/Culture'
import PageNotFound from './components/PageNotFound'
import Geo from './components/Geo'
import './index.css'
import RequireAuth from './admin/components/RequireAuth'
const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)
const token = cookie.load('token')

if (token) {  
  store.dispatch({ type: 'AUTH_USER' })
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path='/' component={App}>
        <IndexRoute component={SliderMain}/>
        <Route path='finishers' component={Finishers}/>
        <Route path='photos' component={Photos}/>
        <Route path='reports' component={Reports}/>
        <Route path='/report/:id' component={Report}/>
        <Route path='registration' component={Request}/>
        <Route path='about' component={About}/>
        <Route path='history' component={History}/>
        <Route path='rules' component={Rules}/>
        <Route path='partners' component={Partners}/>
        <Route path='authors' component={Authors}/>
        <Route path='culture' component={Culture}/>
        <Route path="login" component={Login}/>
        <Route path="logout" component={Logout}/>
        <Route path="register" component={Register}/>
        <Route path="geo" component={Geo}/>
      </Route>
      <Route path='admin' component={RequireAuth(Admin)} >
        <Route path='/finisher/add' component={AddFinisher}/>
        <Route path='finishers' component={AdminFinishers}/>
        <Route path='/start/add' component={AddStart}/>
        <Route path='starts' component={AdminStarts}/>
        <Route path='photo' component={AdminPhoto}/>
        <Route path='partners' component={AdminPartner}/>
        <Route path='request' component={AdminRequest}/>
        <Route path='/photo/add' component={AddPhoto}/>
        <Route path='/partner/add' component={AddPartner}/>
        <Route path='/photo/:id/edit' component={EditPhoto}/>
        <Route path='/admin/finisher/:id/edit' component={EditFinisher}/>
        <Route path='/admin/start/:id/edit' component={EditStart}/>
      </Route>
      <Route path="*" component={PageNotFound}/>
    </Router>
  </Provider>,
  document.getElementById('root')
)
