import React, { Component } from 'react';
import './AddFinisher.css';
import FinisherForm from './FinisherForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as finishersActions from '../../actions/Finishers';
import { browserHistory } from 'react-router';



class AddFinisher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
  }
  handleSubmit(values){
    this.props.finishersActions.addFinisher(values);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/finishers'), 2000);
  }
  render() {
    
    return (
      <div>{!this.state.load ?
        <FinisherForm onSubmit={this.handleSubmit.bind(this)} />
        : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
    added: state.finishers.added
  }
}

function mapDispatchToProps(dispatch){
  return {
    finishersActions: bindActionCreators(finishersActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddFinisher);