import React, { Component } from 'react';
import StartForm from './StartForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as startsActions from '../../actions/Starts';
import { browserHistory } from 'react-router';
import { getFinishers } from '../../actions/Finishers';



class EditStart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
  }
  componentWillMount() {
    this.props.getFinishers();
    this.props.startsActions.getStart(this.props.params.id);
    
  }
  handleSubmit(values){
    this.props.startsActions.addStart(values);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/starts'), 2000);
  }
  render() {
    
    return (
      <div>{!this.state.load ?
        <StartForm onSubmit={this.handleSubmit.bind(this)} finishers={this.props.finishers} />
        : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
    finishers: state.finishers.finishers,
    added: state.finishers.added
  }
}

function mapDispatchToProps(dispatch){
  return {
    getFinishers: bindActionCreators(getFinishers, dispatch),
    startsActions: bindActionCreators(startsActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditStart);