import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import './PhotoForm.css';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';

const renderDropzoneInput = (field) => {
  const files = field.input.value;
  return (
    <div>
      <Dropzone
        name={field.name}
        onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        className="dropzone"
        style={files ? {display: 'none'} : {display: 'block'}}
      >
        <div>Перенеси фото в эту область или кликни для открытия окна с выбором файла</div>
      </Dropzone>
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">{field.meta.error}</span>}
      {files && Array.isArray(files) && (
        
          files.map((file, i) => <img className="photoPreview" key={i} src={file.preview} />)
       
      )}
    </div>
  );
}

class PartnerForm extends Component {
  
  render() {
    
    return (
      <form onSubmit={this.props.handleSubmit} style={{marginTop: '20px'}}>
         <div className="col-md-2">
          <div>
            <label htmlFor='file'>Photo</label>
            <Field
              name='file'
              component={renderDropzoneInput}
            />
          </div>
        </div> 
        <div className="col-md-10">
          <div>
            <label className="formLabel" htmlFor="Name">Name</label>
            <Field className="formField" name='title' component="input" type="text" />
          </div>
          <div>
            <label className="formLabel" htmlFor="Link">Link</label>
            <Field className="formField" name='link' component="input" type="text" />
          </div>
          <div>
            <label className="formLabel" htmlFor="caption">Annotation</label>
            <Field className="textareaField formField" name='annotation' component="textarea" type="text"/>
          </div>
          <button className="btn btn-success" type="submit">Сохранить</button>
        </div>
      </form>
    )
  }
}

PartnerForm = reduxForm({
  form: 'partner'
})(PartnerForm);


export default PartnerForm;