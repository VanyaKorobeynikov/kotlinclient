import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as finishersActions from '../../actions/Finishers';
import './AdminFinishers.css';
import { Link } from 'react-router';  


class AdminFinishers extends Component{
  componentWillMount() {
    this.props.finishersActions.getFinishers();
  }
  componentWillUnmount() {
    this.props.finishersActions.leave();
  }
  handleDelete(id){
    this.props.finishersActions.deleteFinisher(id);
  }
  
  render() {
    
    const finList = this.props.finishers.map((item,index) => {

      let dateStr = new Date(item.created);
      dateStr = ("0" + (dateStr.getDate())).slice(-2)+'-'+("0" + (dateStr.getMonth() + 1)).slice(-2)+'-'+dateStr.getFullYear()+' '+dateStr.getHours()+':'+dateStr.getMinutes()+':'+dateStr.getSeconds();
      let updatedStr = new Date(item.created);
      updatedStr = ("0" + (updatedStr.getDate())).slice(-2)+'-'+("0" + (updatedStr.getMonth() + 1)).slice(-2)+'-'+updatedStr.getFullYear()+' '+updatedStr.getHours()+':'+updatedStr.getMinutes()+':'+updatedStr.getSeconds();

      return (

        <div className="divTableRow" key={index}>
                    <div className="divTableCell">{++index}</div>
                    <div className="divTableCell">{item.name}</div>
                    <div className="divTableCell">{dateStr}</div>
                    <div className="divTableCell">{updatedStr}</div>
                    <div className="divTableCell">{item.city}</div>
                    <div className="divTableCell">{item.country}</div>
                    <div className="divTableCell">{item.age}</div>
                    <div className="divTableCell">{item.sex}</div>
                    <div className="divTableCell">
                      <Link to={'/admin/finisher/'+item._id+'/edit'}>Edit </Link> 
                      <button value={item._id} onClick={() => this.handleDelete(item._id)}>Delete</button> 
                    </div>
        </div>
                
      );
    });


    return (

      <section className="shortcode-item">
        <div className="title"><h2>Финишеры</h2>
          <Link className="btn btn-success" to="finisher/add">Добавить</Link>
        </div>
        {this.props.added 
          ? <div className="addedMessage">Финишер добавлен успешно!</div>
          : ''
        }
        
        <div className="row">
          <div className="divTableAdmin">
            <div className="divTableHeading">
              <div className="divTableRow">
                <div className="divTableCell">#</div>
                <div className="divTableCell">Имя пловца</div>
                <div className="divTableCell">Created</div>
                <div className="divTableCell">Updated</div>
                <div className="divTableCell">Город</div>
                <div className="divTableCell">Страна</div>
                <div className="divTableCell">Возраст</div>
                <div className="divTableCell">Пол</div>
                <div className="divTableCell">Действия</div>
              </div>
            </div>
          <div className="divTableBody">
            {finList}
            </div>
          </div>
          
        </div>
        
      </section>
      

    )
  }
}


function mapStateToProps(state){
  return {
    finishers: state.finishers.finishers,
    fetching: state.finishers.fetching,
    added: state.finishers.added,
  }
}

function mapDispatchToProps(dispatch){
  return {
    finishersActions: bindActionCreators(finishersActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminFinishers);