import React, { Component } from 'react';
import './AddPhoto.css';
import PhotoForm from './PhotoForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as photosActions from '../../actions/Photos';
import { getFinishers } from '../../actions/Finishers';
import { getStarts } from '../../actions/Starts';
import { browserHistory } from 'react-router';




class AddPhoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
  }
  handleSubmit(photo){
    console.log(photo);
    this.props.photosActions.addPhoto(photo);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/photo'), 2000);
  }
  componentWillMount() {
    this.props.getFinishers();
    this.props.getStarts();
  }
  render() {
    return (
      <div>{!this.state.load ?
        <PhotoForm onSubmit={this.handleSubmit.bind(this)} starts={this.props.starts} finishers={this.props.finishers}/>
        : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
    finishers: state.finishers.finishers,
    starts: state.starts.starts
  }
}

function mapDispatchToProps(dispatch){
  return {
    photosActions: bindActionCreators(photosActions, dispatch),  
    getFinishers: bindActionCreators(getFinishers, dispatch),
    getStarts: bindActionCreators(getStarts, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPhoto);