import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import './StartForm.css';

class StartForm extends Component {
  render() {
    return (
      <form onSubmit={this.props.handleSubmit} className="form">

        <div>
          <Field className="selectField formField" name="_id" component="input" type="hidden" />
        </div>
        <div>
          <label className="formLabel" htmlFor="name">Название</label>
          <Field className="selectField formField" name="name" component="input" />
        </div>
        <div>
          <label className="formLabel" htmlFor="about">О заплыве</label>
          <Field className="textareaField formField" name="about" component="textarea" type="text" wrap="hard"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="time">Time</label>
          <Field className="formField" name="time" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="date">Дата(ФОРМАТ YYYY-MM-DD):</label>
          <Field className="formField" name="date" component="input" type="text"/>
        </div><br />
        <div>
          <label className="formLabel" htmlFor="wheather">Wheather</label>
          <Field className="textareaField formField" name="wheather" component="textarea" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="food">Food</label>
          <Field className="textareaField formField" name="food" component="textarea" type="text"/>
        </div>                
        <div>
          <label className="formLabel" htmlFor="type">Type</label>
          <Field className="selectField formField" name="type" component="select">
            <option></option>
            <option value="solo">Solo</option>
            <option value="relay">Relay</option>
          </Field>
        </div>
        
        <div>
            <label className="formLabel">Финишеры</label>
            <div>
              <Field className ="textareaField formField" component="select" name='finishers' multiple size="35">
                {this.props.finishers.map(item => <option key={item._id} value={item._id}>{item.name}</option>)}
              </Field>
            </div>
        </div>
        <button className="btn btn-success" type="submit">Сохранить</button>
      </form>
    )
  }
}

StartForm = reduxForm({
  form: 'start',
  initialValues: {finishers: []}
})(StartForm);

export default StartForm;