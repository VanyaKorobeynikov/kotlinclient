import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as PhotosActions from '../../actions/Photos';
import './AdminPhoto.css';
import { Link } from 'react-router';  


class AdminPhoto extends Component{
  componentWillMount() {
    this.props.photosActions.getPhotos(1);
  }
  handleDelete(id) {
    this.props.photosActions.deletePhoto(id);
  }
  
  render() {
    const photos = this.props.photos.map((item,index) => {
      return ( 
        <div className='adminPhoto' key={index}>
          <Link to={'/photo/'+item._id+'/edit'}>
            <div>
              <img src={'/images/gallery/'+item.name} alt={item.caption} />
              <div>Подпись: {item.caption}</div>
              <div>Год: {item.year}</div>
              {item.finisher ? <div>Финишер: {item.finisher.name}</div> : null}
              {item.start ? <div>Старт: {item.start.name}</div> : null}
            </div>
          </Link>
          <button value={item._id} onClick={() => this.handleDelete(item._id)}>Delete</button> 
        </div>
      )
    })
    return (

      <section className="shortcode-item">
        <div className="title"><h2>Фото</h2>
          <Link className="btn btn-success" to="photo/add">Добавить</Link>
        </div>
        
        <div className="flex-container">
        {photos}
        </div>
      </section>
      

    )
  }
}


function mapStateToProps(state){
  return {
    photos: state.photos.photos,
    fetching: state.photos.fetching,
    year: state.photos.year,
  }
}

function mapDispatchToProps(dispatch){
  return {
    photosActions: bindActionCreators(PhotosActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminPhoto);