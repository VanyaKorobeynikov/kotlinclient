import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as requestActions from '../../actions/Request';
import { Link } from 'react-router';  


class AdminRequest extends Component{
  componentWillMount() {
    this.props.requestActions.getRequests();
  }
  // componentWillUnmount() {
  //   this.props.finishersActions.leave();
  // }
  handleDelete(id){
    //e.preventDefault();
    this.props.requestActions.deleteRequest(id);
  }
  
  render() {
    
    const finList = this.props.requests.map((item,index) => {

      let dateStr = new Date(item.created);
      dateStr = ("0" + (dateStr.getDate())).slice(-2)+'-'+("0" + (dateStr.getMonth() + 1)).slice(-2)+'-'+dateStr.getFullYear()+' '+dateStr.getHours()+':'+dateStr.getMinutes()+':'+dateStr.getSeconds();
      // let updatedStr = new Date(item.created);
      // updatedStr = ("0" + (updatedStr.getDate())).slice(-2)+'-'+("0" + (updatedStr.getMonth() + 1)).slice(-2)+'-'+updatedStr.getFullYear()+' '+updatedStr.getHours()+':'+updatedStr.getMinutes()+':'+updatedStr.getSeconds();

      return (

        <div className="divTableRow" key={index}>
                    <div className="divTableCell">{++index}</div>
                    <div className="divTableCell">{item.name}</div>
                    <div className="divTableCell">{item.contact}</div>
                    <div className="divTableCell">{item.date}</div>
                    <div className="divTableCell">{item.type}</div>
                    <div className="divTableCell">{item.age}</div>
                    <div className="divTableCell">{item.comment}</div>
                    <div className="divTableCell">{item.country}</div>
                    <div className="divTableCell">{dateStr}</div>
                    <div className="divTableCell">
                      <Link to={'/admin/finisher/'+item._id+'/edit'}>Edit </Link> 
                      <button value={item._id} onClick={() => this.handleDelete(item._id)}>Delete</button> 
                    </div>
        </div>
                
      );
    });


    return (

      <section className="shortcode-item">
        <div className="title"><h2>Запросы на регистрацию</h2>
          <Link className="btn btn-success" to="finisher/add">Добавить</Link>
        </div>
        {this.props.added 
          ? <div className="addedMessage">Финишер добавлен успешно!</div>
          : ''
        }
        
        <div className="row">
          <div className="divTableAdmin">
            <div className="divTableHeading">
              <div className="divTableRow">
                <div className="divTableCell">#</div>
                <div className="divTableCell">Имя</div>
                <div className="divTableCell">Контакт</div>
                <div className="divTableCell">Дата</div>
                <div className="divTableCell">Тип гонки</div>
                <div className="divTableCell">Возраст</div>
                <div className="divTableCell">Комментарий</div>
                <div className="divTableCell">Страна</div>
                <div className="divTableCell">Created</div>
                <div className="divTableCell">Действия</div>
              </div>
            </div>
          <div className="divTableBody">
            {finList}
            </div>
          </div>
          
        </div>
        
      </section>
      

    )
  }
}


function mapStateToProps(state){
  return {
    requests: state.request.requests,
    fetching: state.finishers.fetching
  }
}

function mapDispatchToProps(dispatch){
  return {
    requestActions: bindActionCreators(requestActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminRequest);