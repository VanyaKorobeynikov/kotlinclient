import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Dropzone from 'react-dropzone';
import './FinisherForm.css';

const renderDropzoneInput = (field) => {
  const files = field.input.value;
  return (
    <div>
      <Dropzone
        name={field.name}
        onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        className="dropzone"
        style={files ? {display: 'none'} : {display: 'block'}}
      >
        <div>Перенеси фото в эту область или кликни для открытия окна с выбором файла</div>
      </Dropzone>
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">{field.meta.error}</span>}
      {files && Array.isArray(files) && (
        
          files.map((file, i) => <img className="photoPreview" key={i} src={file.preview} />)
       
      )}
    </div>
  );
}

class FinisherForm extends Component {
	
  render() {
    return (
      <form onSubmit={this.props.handleSubmit} className="form">
      <div className="col-md-2">
          <div>
            <label htmlFor='photo'>Photo</label>
            {(this.props.finisher && this.props.finisher.photo != '')? 
              <img style={{width: '100%'}} src={'../../../../images/finishers/'+this.props.finisher.photo} alt="preview"/>
            : 
            <Field
              name='photo'
              component={renderDropzoneInput}
            />

          }
          </div>
         
      </div> 
      <div className="col-md-10">
        <div>
          <Field className="formField" name="_id" component="input" type="hidden"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="name">Name</label>
          <Field className="formField" name="name" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="about">About</label>
          <Field className="textareaField formField" name="about" component="textarea" rows="10" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="sport">Sport</label>
          <Field className="formField" name="sport" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="city">City</label>
          <Field className="formField" name="city" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="country">Country</label>
          <Field className="formField" name="country" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="age">Age</label>
          <Field className="formField" name="age" component="input" type="text"/>
        </div>
        <div>
        <label className="formLabel">Gender</label>
          <div>
            <Field className="selectField formField" name="sex" component="select">
              <option></option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </Field>
          </div>
        </div>
      </div>  
        <button className="btn btn-success" type="submit">Добавить</button>
      </form>
    )
  }
}

FinisherForm = reduxForm({
  form: 'finisher',
})(FinisherForm);

export default FinisherForm;