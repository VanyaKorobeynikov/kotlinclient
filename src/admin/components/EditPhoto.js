import React, { Component } from 'react';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import * as PhotosActions from '../../actions/Photos';
import './EditPhoto.css';
import { Link } from 'react-router';
import EditPhotoForm from './EditPhotoForm';  
import { getFinishers } from '../../actions/Finishers';
import { getStarts } from '../../actions/Starts';
import { browserHistory } from 'react-router';


class EditPhoto extends Component{
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
    let { photo, initializePhoto, finishers } = this.props;   
  }
  componentWillMount() {
    this.props.getFinishers();
    this.props.getStarts();
    this.props.photosActions.getPhoto(this.props.params.id);

  }
  handleSubmit(values) {
    this.props.photosActions.editPhoto(values);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/photo'), 2000);
    
  }
  
  render() {
    console.log(this.props.photo);
    return ( 
      <div>{!this.state.load ?
        <EditPhotoForm photo={this.props.photo} finishers={this.props.finishers} starts={this.props.starts} onSubmit={this.handleSubmit.bind(this)}/>
         : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
    photo: state.photos.photo,
    fetching: state.photos.fetching,
    year: state.photos.year,
    finishers: state.finishers.finishers,
    starts: state.starts.starts
  }
}

function mapDispatchToProps(dispatch){
  return {
    initializePhoto: function (photo){
            dispatch(initialize('editPhoto', photo));
    },
    photosActions: bindActionCreators(PhotosActions, dispatch),
    getFinishers: bindActionCreators(getFinishers, dispatch),
    getStarts: bindActionCreators(getStarts, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPhoto);