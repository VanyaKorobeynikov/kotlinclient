import React, { Component } from 'react';
import './AddFinisher.css';
import FinisherForm from './FinisherForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as finishersActions from '../../actions/Finishers';
import { browserHistory } from 'react-router';



class EditFinisher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
  }
  componentWillMount() {
    this.props.finishersActions.getFinisher(this.props.params.id);
    
  }
  handleSubmit(values){
    this.props.finishersActions.addFinisher(values);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/finishers'), 2000);
  }
  render() {
    
    return (
      <div>{!this.state.load ?
        <FinisherForm onSubmit={this.handleSubmit.bind(this)} finisher={this.props.finisher} />
        : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
    finisher: state.finishers.finisher,
    added: state.finishers.added
  }
}

function mapDispatchToProps(dispatch){
  return {
    finishersActions: bindActionCreators(finishersActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditFinisher);