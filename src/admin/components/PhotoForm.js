import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import './PhotoForm.css';
import * as PhotosActions from '../../actions/Photos';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';

const renderDropzoneInput = (field) => {
    console.log(field);
  const files = field.input.value;
  return (
    <div>
      <Dropzone
        name={field.name}
        onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        className="dropzone"
        style={files ? {display: 'none'} : {display: 'block'}}
      >
        <div>Перенеси фото в эту область или кликни для открытия окна с выбором файла</div>
      </Dropzone>
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">{field.meta.error}</span>}
      {files && Array.isArray(files) && (
        
          files.map((file, i) => <img className="photoPreview" key={i} src={file.preview} />)
       
      )}
    </div>
  );
}

class PhotoForm extends Component {
  
  render() {
    
    return (
      <form onSubmit={this.props.handleSubmit} style={{marginTop: '20px'}}>
      
        <div className="col-md-2">
          <div>
            <label htmlFor='file'>Photo</label>
            <Field
              name='file'
              component={renderDropzoneInput}
            />
          </div>
         
        </div> 
        <div className="col-md-10">
          <div>
            <label className="formLabel" htmlFor="finisher">Finisher</label>
            <Field className="formField" name='finisher' component="select" type="text">
            <option value="noselect">select finisher</option>
            {this.props.finishers.map((item) => 
              <option value={item._id} key={item._id}>{item.name}</option>
            )}
            </Field>
          </div>
          <div>
            <label className="formLabel" htmlFor="start">Start</label>
            <Field className="formField" name='start' component="select" type="text">
            <option value="noselect">select start</option>
            {this.props.starts.map((item) => 
              <option value={item._id} key={item._id}>{item.name}</option>
            )}
            </Field>
          </div>
          <div>
            <label className="formLabel" htmlFor="Year">Year</label>
            <Field className="formField" name='year' component="input" type="text" />
          </div>
          <br />
          <div>
            <label className="formLabel" htmlFor="caption">Caption</label>
            <Field className="textareaField formField" name='caption' component="textarea" type="text"/>
          </div>
          <button className="btn btn-success" type="submit">Сохранить</button>
        </div>
        
      </form>
    )
  }
}

PhotoForm = reduxForm({
  form: 'photo'
})(PhotoForm);


export default PhotoForm;