import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as PartnersActions from '../../actions/Partners';
import './AdminPhoto.css';
import { Link } from 'react-router';  


class AdminPartner extends Component{
  componentWillMount() {
    this.props.partnersActions.getPartners();
  }
  handleDelete(id) {
    //this.props.photosActions.deletePhoto(id);
  }
  
  render() {
    const partners = this.props.partners.map((item,index) => {
      return ( 
        <div className='adminPhoto' key={index}>
          <Link to={'/partner/'+item._id+'/edit'}>
            <div>
              <img src={'/images/partners/'+item.name} alt={item.caption} />
              <div>Подпись: {item.caption}</div>
              <div>Год: {item.year}</div>
              {item.finisher ? <div>Name: {item.partner.title}</div> : null}
              {item.start ? <div>Annotation: {item.start.annotation}</div> : null}
            </div>
          </Link>
          <button value={item._id} onClick={() => this.handleDelete(item._id)}>Delete</button> 
        </div>
      )
    })
    return (

      <section className="shortcode-item">
        <div className="title"><h2>Партнеры</h2>
          <Link className="btn btn-success" to="partner/add">Добавить</Link>
        </div>
        
        <div className="flex-container">
        {partners}
        </div>
      </section>
      

    )
  }
}


function mapStateToProps(state){
  return {
    partners: state.partners.partners,
    fetching: state.partners.fetching,
  }
}

function mapDispatchToProps(dispatch){
  return {
    partnersActions: bindActionCreators(PartnersActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminPartner);