import React, { Component } from 'react';
import './AddStart.css';
import StartForm from './StartForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as startsActions from '../../actions/Starts';
import { getFinishers } from '../../actions/Finishers';
import { browserHistory } from 'react-router';





class AddStart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
  }
  componentWillMount() {
    this.props.getFinishers();
  }
  handleSubmit(values){
    this.props.startsActions.addStart(values);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/starts'), 2000);
  }
  componentWillMount() {
    this.props.getFinishers();
  }
  render() {
    return (
      <div>{!this.state.load ?
        <StartForm onSubmit={this.handleSubmit.bind(this)} finishers={this.props.finishers} />
        : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
    finishers: state.finishers.finishers
  }
}

function mapDispatchToProps(dispatch){
  return {
    getFinishers: bindActionCreators(getFinishers, dispatch),
    startsActions: bindActionCreators(startsActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddStart);