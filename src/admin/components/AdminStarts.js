import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as startsActions from '../../actions/Starts';
import './AdminStarts.css';
import { Link } from 'react-router';  


class AdminStarts extends Component{
  componentWillMount() {
    this.props.startsActions.getStarts();
  }
  handleDelete(id){
    //e.preventDefault();
    this.props.startsActions.deleteStart(id);
  }
  render() {
    
    const startList = this.props.starts.map((item,index) => {
      const date = new Date(item.date);
      const dateStr = date.getFullYear() +'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
      return (

        <div className="divTableRow" key={index}>
                    <div className="divTableCell">{++index}</div>
                    <div className="divTableCell">{item.name}</div>
                    <div className="divTableCell">{dateStr}</div>
                    <div className="divTableCell">{item.time}</div>
                    <div className="divTableCell">
                      <Link to={'/admin/start/'+item._id+'/edit'}>Edit </Link> 
                      <button value={item._id} onClick={() => this.handleDelete(item._id)}>Delete</button> 
                    </div>
        </div>
                
      );
    });


    return (

      <section className="shortcode-item">
        <div className="title"><h2>Старты</h2>
          <Link className="btn btn-success" to="start/add">Добавить</Link>
        </div>
        {this.props.deleting ?
          <div className="loader"></div>
          :
        <div className="row">
          <div className="divTableAdmin">
            <div className="divTableHeading">
              <div className="divTableRow">
                <div className="divTableCell">#</div>
                <div className="divTableCell">Название заплыва</div>
                <div className="divTableCell">Дата</div>
                <div className="divTableCell">Время</div>
                <div className="divTableCell">Действия</div>
              </div>
            </div>
          <div className="divTableBody">
            {startList}
          </div>
          </div>
          
        </div> }
        
      </section>
      

    )
  }
}


function mapStateToProps(state){
  return {
    starts: state.starts.starts,
    fetching: state.starts.fetching,
    deleting: state.starts.deleting
  }
}

function mapDispatchToProps(dispatch){
  return {
    startsActions: bindActionCreators(startsActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminStarts);