import React, { Component } from 'react';
import './AddPhoto.css';
import PartnerForm from './PartnerForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import * as partnersActions from '../../actions/Partners';



class AddPartner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false
    }
  }
  handleSubmit(values){
    this.props.partnersActions.addPartner(values);
    this.setState({load: true});
    setTimeout(() => browserHistory.push('/admin/partners'), 2000);
  }

  render() {
    return (
      <div>{!this.state.load ?
        <PartnerForm onSubmit={this.handleSubmit.bind(this)} starts={this.props.starts} finishers={this.props.finishers}/>
        : <div className="loader"></div>}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {
  }
}

function mapDispatchToProps(dispatch){
  return {
    partnersActions: bindActionCreators(partnersActions, dispatch),  
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPartner);