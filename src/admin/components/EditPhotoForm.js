import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import './PhotoForm.css';
import * as PhotosActions from '../../actions/Photos';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';


class EditPhotoForm extends Component {
  
  render() {
    
    return (
      <form onSubmit={this.props.handleSubmit} style={{marginTop: '20px'}}>
      
        <div>
          <Field name="_id" type="hidden" component="input"/>
          <img className="col-md-2" src={'/images/gallery/'+this.props.photo.name} alt="preview"/>
        </div> 
        <div className="col-md-10">
          <div>
            <label className="formLabel" htmlFor="finisher">Finisher</label>
            <Field className="formField" name='finisher._id' component="select" type="text">
            <option value="noselect">select finisher</option>
            {this.props.finishers.map((item) => 
              <option value={item._id} key={item._id}>{item.name}</option>
            )}
            </Field>
          </div>
          <div>
            <label className="formLabel" htmlFor="start">Start</label>
            <Field className="formField" name='start' component="select" type="text">
            <option value="noselect">select start</option>
            {this.props.starts.map((item) => 
              <option value={item._id} key={item._id}>{item.name}</option>
            )}
            </Field>
          </div>
          <div>
            <label className="formLabel" htmlFor="Year">Year</label>
            <Field className="formField" name='year' component="input" type="text" />
          </div>
          <br />
          <div>
            <label className="formLabel" htmlFor="caption">Caption</label>
            <Field className="textareaField formField" name='caption' component="textarea" type="text"/>
          </div>
          <button className="btn btn-success" type="submit">Сохранить</button>
        </div>
        
      </form>
    )
  }
}

EditPhotoForm = reduxForm({
  form: 'editPhoto'
})(EditPhotoForm);


export default EditPhotoForm;