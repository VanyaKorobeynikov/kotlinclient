import React, { Component } from 'react'
import { Link } from 'react-router'
import './Admin.css'

const Admin = (props) => (
  <div>
    <div className="header" id="header">
    <h1 className="site_title" id="site_title">Kotlin</h1>
    <ul className="header-item tabs" id="tabs">
      <li id="finishers"><Link  activeClassName="active" to="/admin/finishers">Finishers</Link></li>
      <li id="photo"><Link to="/admin/photo">Images</Link></li>
      <li id="starts"><Link to="/admin/starts">Starts</Link></li>
      <li id="starts"><Link to="/admin/request">Request</Link></li>
      <li id="partners"><Link to="/admin/partners">Partners</Link></li>
      <li id="main"><Link to="/">на главную</Link></li>
    </ul>
    <ul className="header-item tabs" id="utility_nav">
      <li id="logout"><Link to="logout">Logout</Link></li>
    </ul>
  	</div>
    {props.children}
  </div>
)

export default Admin