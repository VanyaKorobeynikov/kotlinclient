import React from 'react'
import { Link } from 'react-router'
import { SocialIcon } from 'react-social-icons'
import './Footer.css';

const Footer = () => (
    <footer className="footer" id="footer">
      <div className="container">
          <div className="row">
              <div className="col-sm-6 col-xs-12">
                  © 2014-2018 Kotlinrace
                  <SocialIcon url="https://vk.com/kotlinrace" style={{ height: 25, width: 25, marginLeft: 10 }}  />
                  <SocialIcon url="https://www.facebook.com/kotlinrace/" style={{ height: 25, width: 25, marginLeft: 10 }}  />
                  <SocialIcon url="https://www.instagram.com/kotlinrace/" style={{ height: 25, width: 25, marginLeft: 10 }}  />
              </div>
              <div className="col-sm-6 col-xs-12">
                  <ul className="pull-right">
                      <li>
                          <Link to="/about" className="ember-view"  id="ember479">О заплыве</Link>
                      </li>
                      <li>
                          <Link to="/register" className="btn ember-view"  id="ember480">Зарегистрироваться</Link>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>
)

export default Footer