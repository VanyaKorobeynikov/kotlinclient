import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as finishersActions from '../actions/Finishers';
import { getStarts } from '../actions/Starts';
import './Finishers.css';
import FinisherRow from './FinisherRow';
import { Link } from 'react-router';


class Finishers extends Component{
  constructor(props) {
    super(props);
    this.state = {
      starts: this.props.starts
    }
  }
  componentWillMount() {
    this.props.getStarts();
  }
  componentDidMount(){
    document.title = "KotlinRace - Финишеры";
  }
  handleTypeClick(e){
    const type = (e.target.innerText == "Соло") ? "solo" :"relay";
    this.props.getStarts(type);    
  }
  render() {
    let startList = this.props.starts.map((item) => {
      let dateStr = new Date(item.date);
      dateStr = ("0" + (dateStr.getDate())).slice(-2)+'-'+("0" + (dateStr.getMonth() + 1)).slice(-2)+'-'+dateStr.getFullYear();
      return (
        <div className="card row" key={item._id}>
          <div className="card-heading col-xs-12 col-sm-3 col-lg-2">
            <div className="col-xs-7 col-sm-12"><p>Дата:</p><p>{dateStr}</p></div>
            <div className="col-xs-5 col-sm-12"><p>Время:</p><p>{item.time}</p></div>
          </div>
          <div className="card-body col-xs-12 col-sm-9 col-lg-10">
          {item.finishers && item.finishers.length > 0  ? <p style={{fontSize: '24px'}}>Финишеры:</p> : null}
          {item.finishers ? item.finishers.map(finisher => 
            (<div key={finisher._id} className="row">
              <div><span className="finisherName col-xs-12 col-sm-12 col-lg-4">{finisher.name}</span> <span className="col-xs-12 col-sm-12 col-lg-8">Возраст: {finisher.age} Страна: {finisher.country} Город: {finisher.city}</span></div>
            </div>)
          ): null}
          <Link to={`/report/${item._id}`}>подробнее...</Link>
          </div>
        </div>
      )
    })
    const finSoloList = this.props.finishers.map((item,index) => {
      if(item.starts.length > 0 && item.starts[0].type === 'solo') {
      
      return (
       <FinisherRow item={item} start={item.starts[0]} key={index} />
      );
    }else{ 
      return '';}
    });
    const finRelayList = this.props.finishers.map((item,index) => {
      for(let i=0; i<item.starts.length; i++){
      
      if(item.starts.length > 0 && item.starts[i].type === 'relay') {

      return (
        <FinisherRow item={item} start={item.starts[0]} key={index} />  
      );
    }else{ return '';}}
    });


    return (

      <section>
        <div className="container">
            <div className="center">
               <h2>Финишеры</h2>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-6 col-sm-offset-4">
                <button onClick={this.handleTypeClick.bind(this)} className="col-xs-5 col-md-4 type-button ">Соло</button>
                <button onClick={this.handleTypeClick.bind(this)} className="col-xs-5 col-md-4 type-button ">Эстафета</button>
              </div>
            </div>

            {startList}
    
            
          </div>
      </section>
      

    )
  }
}

function mapStateToProps(state){
  return {
    finishers: state.finishers.finishers,
    starts: state.starts.starts,
    fetching: state.finishers.fetching
  }
}

function mapDispatchToProps(dispatch){
  return {
    finishersActions: bindActionCreators(finishersActions, dispatch),
    getStarts: bindActionCreators(getStarts, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Finishers);