import React from 'react';
import {shallow} from 'enzyme';
import FinisherRow from './FinisherRow';
      
it('Render table-row with finisher-data', () => {
  const item = {
        name: 'Ivan Korobeynikov',
        city: 'Orsha',
        country: 'BY',
        age: '25',
        sex: 'Male'
      };
  const start = {
        wetsuite: true,
        date: '2015-07-16',
        time: '6:15:16',
        type: 'solo'
      };

  const row = shallow(
    <FinisherRow item={item} start={start} />
  );

  expect(row.find('.divTableRow').contains([
    <div className='divTableCell'>Ivan Korobeynikov</div>,
    <div className='divTableCell'>Orsha</div>,
    <div className='divTableCell'>BY</div>,
    <div className='divTableCell'>25</div>,
    <div className='divTableCell'>Male</div>,
    <div className='divTableCell'>yes</div>,
    <div className='divTableCell'>2015-07-16</div>,
    <div className='divTableCell'>6:15:16</div>,
    <div className='divTableCell'>solo</div>])).toEqual(true);
});

  