import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addRequest, leave } from '../actions/Request';
import { Link } from 'react-router';
import RequestForm from './RequestForm';

class Request extends Component{
  handleSubmit(values) {
    this.props.addRequest(values);
  }
  componentWillUnmount() {
    this.props.leave();
  }
  render() {

    return (

      <section id="blog" className="container">
        {this.props.added ? 
        <div className="row">
          <div className="col-md-8 col-md-offset-2">
            <h2>Ваша заявка принята.</h2>
            <h3>В ближайшее время с вами свяжутся по указанным контактным данным. Спасибо.</h3>
          </div>
        </div>  
          :
        <div className="row">
          <div className="col-md-8 col-md-offset-2">
            <div id="contact-page clearfix">
                <div className="message_heading">
                    <h2>Форма отравки заявки на участие</h2>
                    <p>Убедитесь что все обязательные поля (помеченые *) заполнены.</p>
                </div> 
              <RequestForm onSubmit={this.handleSubmit.bind(this)} />
            </div>
          </div>
        </div>
        }
      </section>
    )
  }
}

function mapStateToProps(state){
  return {
    request: state.request.request,
    fetching: state.request.fetching,
    added: state.request.added
  }
}

function mapDispatchToProps(dispatch){
  return {
    addRequest: bindActionCreators(addRequest, dispatch),
    leave: bindActionCreators(leave, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Request);