import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFinishers } from '../actions/Finishers';
import { YMaps, Map, ObjectManager } from 'react-yandex-maps';


class Geo extends Component{
  componentWillMount() {
    this.props.getFinishers();
  }
  componentDidMount(){
    document.title = "KotlinRace - География";
  }

  render() {
    const mapState = { center: [53.902257, 27.561831], zoom: 4, autoFitToViewport: 'always'};
    const filtered = this.props.finishers.filter((item) => (Boolean(item.marker)))
    const data = filtered.map((item) => {
      let starts = item.starts.map((start) => (
        `<div>
           <h3>Старты:</h3>
           <a href=
         </div>`
      ))
      return {
        geometry: {
          coordinates: [
            item.marker.lat, 
            item.marker.lon
          ],
          type: 'Point'
        },
        id: item._id,
        properties: {
          balloonContent: `<div>
                             <h2>${item.name}</h2>
                             <span>Возраст: ${item.age}</span> <span>Пол: ${item.sex}</span> <span>Страна: ${item.country}</span>
                           </div>`,
          clusterCaption: item.name,
          hintContent: item.name
        },
        type: 'Feature'
      }
    })

    return (

      <section>
        <div className="container">
            <div className="center">
               <h2>Карта</h2>
            </div>
         <YMaps style={{ width: '600px'}}>
          <Map state={mapState} width='100%' height="500">
            <ObjectManager
              options={{
                clusterize: true,
                gridSize: 32,
                clusterDisableClickZoom: true
              }}
              objects={{
                preset: 'islands#greenDotIcon',
              }}
              clusters={{
                preset: 'islands#greenClusterIcons',
              }}
              features={data}
            />
          </Map>
        </YMaps>
        </div>

      </section>
      

    )
  }
}

function mapStateToProps(state){
  return {
    finishers: state.finishers.finishers,
  }
}

function mapDispatchToProps(dispatch){
  return {
    getFinishers: bindActionCreators(getFinishers, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Geo);