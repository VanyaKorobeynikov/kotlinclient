import React, { Component } from 'react';
import './Photos.css';
import Gallery from './Gallery';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';    //const filtered = photos
import * as photosActions from '../actions/Photos';

class Photos extends Component{
  componentWillMount() {
    this.props.photosActions.getPhotos(1);
  }
  handleYearClick(y) {
    this.props.photosActions.filterPhoto(y)
  }

  render() {
    const { photos, filteredPhotos } = this.props
    const photosForGallery = filteredPhotos.map(({ name, year, caption }, index) => ({
      src: `/images/gallery/${name}`,
      caption: `${caption}. ${year}`,
      alt: caption
    }))
    const years = photos
    .reduce((prev, { year }) => {
      if (prev.indexOf(year) > -1) return [ ...prev ]

      return [ ...prev, year ]
    }, [ 0 ])
    .sort((a, b) => (a-b))

    return (
      <div className="container">
        <div className="center">
             <h2>Финишеры</h2>
          </div>
          <div className="row">
            <div className="col-sm-8 col-sm-offset-2 year-row">
            {years.map((item, index) => (
              <button 
                onClick={() => (this.handleYearClick(item))} 
                key={index} 
                className="year-button">
                {item > 0 ? item : 'Все'}
              </button>
            ))}
            </div>
          </div>
        {this.props.year > 2000 ?
          <h2>{this.props.year} год</h2>
          : null
        }  
        {this.props.fetching ? 
          <div className=".loader"></div>
          : 
          <Gallery photos={photosForGallery}/>
        }
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    photos: state.photos.photos,
    filteredPhotos: state.photos.filteredPhotos,
    fetching: state.photos.fetching,
    year: state.photos.year
  }
}

function mapDispatchToProps(dispatch){
  return {
    photosActions: bindActionCreators(photosActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Photos);