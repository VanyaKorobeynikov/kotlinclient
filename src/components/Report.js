import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getStart } from '../actions/Starts';
import { Link } from 'react-router';
import './Report.css';

class Reports extends Component{
  componentWillMount() {
    this.props.getStart(this.props.params.id);
  }
  render() {
    const { name, about, date, food, wheather, time, type, finishers, photos } = this.props.start;
    let dateStr = new Date(date);
    dateStr = ("0" + (dateStr.getDate())).slice(-2)+'-'+("0" + (dateStr.getMonth() + 1)).slice(-2)+'-'+dateStr.getFullYear();
    return (
      <div className="container">
      {this.props.fetching ? 
        <div className="loader"></div>
        :
          finishers ?
          <div>
          <section id="blog" className="col-lg-10 col-md-8 col-xs-12">

            <div className="center">
              <h2>{name}</h2>
            </div>
            <div style={{whiteSpace: "pre-wrap"}}>
              <span style={{fontSize: "16px"}}><b>Дата:</b> {dateStr} <b>Время:</b> {time} <b>Тип:</b> {type} </span>
              <div>
                <p>{about}</p>
              </div>
              <div className="col-md-6 col-lg-6 col-xs-12">
                <b>Финишеры:</b><br />
                  {finishers ? finishers.map(item => (
                  <p className="finisherName" key={item._id}>{item.name}</p>
                  )) : <div className="loader"></div>}
                <b>Питание:</b><br />{food}<br />

              </div>
              <div className="col-md-6 col-lg-6 col-xs-12">
                <b>Погода:</b><br />{wheather}<br />
              </div>
            </div>

            <br />
          
        </section>
        <section className="col-lg-2 col-md-4 col-xs-12">
          <div className="row">
          {photos.map(item => (<div className="col-md-6 col-lg-12 col-xs-12" key={item._id} ><img  alt={item.caption} width="100%" src={'/images/gallery/'+item.name} /></div>))}
          </div>
          </section>
        </div> : <div className="loader"></div>}
        </div>
    )
  }
}

function mapStateToProps(state){
  return {
    start: state.starts.start,
    fetching: state.starts.fetching
  }
}

function mapDispatchToProps(dispatch){
  return {
    getStart: bindActionCreators(getStart, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reports);