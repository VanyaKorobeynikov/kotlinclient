import React from 'react'

const Partner = (props) => {
  const { partner } = props

  return (
      <a
        href={partner.link}
        className="photo"
        target="_blank"
        style={{
          background: `url('/images/partners/${partner.name}')`,
          backgroundSize: 'contain',
          backgroundRepeat: 'no-repeat',
        }}
      >
      <div className="photoCaption">
      {partner.annotation}       
      </div></a>
  )
}

export default Partner