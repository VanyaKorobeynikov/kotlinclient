import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getStarts } from '../actions/Starts';
import { Link } from 'react-router';

class Reports extends Component{
  componentWillMount() {
    this.props.getStarts();
  }
  render() {
    const reportList = this.props.starts.map(item => {
      const date = new Date(item.date);
      const preview = item.about ? item.about.substr(0,300): null;
      return (
        
        <div className="blog" key={item._id}>
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              
                <div className="blog-item">
                  <div className="row">
                    <div className="col-xs-12 col-sm-2 text-center">
                      <div className="entry-meta">
                        <span id="publish_date">{("0" + (date.getDate())).slice(-2) +'.'+("0" + (date.getMonth() + 1)).slice(-2)+'.'+date.getFullYear()}</span>
                        
                      </div>
                    </div>
                        
                    <div className="col-xs-12 col-sm-10 blog-content">
                      
                      <h2>{item.name}</h2>
                      <h3>{preview}...</h3>

                        <p><Link to={'/report/'+item._id}>Далее...</Link></p>
                      
                    </div>
                  </div>    
                </div>
              
            </div>

          </div>
        </div>
      )
    })

    return (
      <section id="blog" className="container">
        <div className="center">
          <h2>Отчеты участников</h2>
        </div>
        {reportList}
        <br />
    </section>
    )
  }
}

function mapStateToProps(state){
  return {
    starts: state.starts.starts,
    fetching: state.finishers.fetching
  }
}

function mapDispatchToProps(dispatch){
  return {
    getStarts: bindActionCreators(getStarts, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reports);