import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import './RequestForm.css';

class RequestForm extends Component {
  render() {
    return (
      <form onSubmit={this.props.handleSubmit} className="row">
        <div className="reqCol col-md-6">
          <div className="">
            <label className="reqLabel" htmlFor="name">Ваше имя *</label>
            <Field className="form-control" name="name" component="input" placeholder="пример: Дмитрий Иванович Пловцов"/>
          </div>
          <div>
            <label className="" htmlFor="date">Предполагаемая дата заплыва *</label>
            <Field className="form-control" name="date" component="input" placeholder="пример: лето 2017/конец июля"/>
          </div>
          <div>
            <label className="" htmlFor="age">Возраст *</label>
            <Field className="form-control" name="age" component="input" />
          </div>
          <div>
            <label className="" htmlFor="country">Страна</label>
            <Field className="form-control" name="country" component="input" />
          </div>
        </div>
        <div className="reqCol col-md-6">
          <div>
            <label className="" htmlFor="contact">Контакт (телефон/email) *</label>
            <Field className="form-control" name="contact" component="input" placeholder="пример: +79211234567/marathoner@kotlinrace.ru"/>
          </div>
          <div>
            <label className="" htmlFor="type">Тип гонки</label>
            <Field className="form-control" name="type" component="select">
              <option>--выбрать--</option>
              <option value="solo">Соло</option>
              <option value="relay">Эстафета</option>
            </Field>
          </div>
          <div>
            <label className="" htmlFor="comment">Комментарий</label>
            <Field className="form-control" name="comment" component="textarea" type="text" rows="4" wrap="hard"/>
          </div>
        </div>
        
        
        <div className="clearfix"></div>
        <br />
        <button className="btn btn-success reqBtn" type="submit">Отправить заявку</button>
      </form>
    )
  }
}

RequestForm = reduxForm({
  form: 'request',
})(RequestForm);

export default RequestForm;