import React, { Component } from 'react';
import Slider from 'react-slick';
import './Slider.css';

class SliderMain extends Component {
  render() {
    const settings = {
      dots: false,
      arrows: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      accessibility: true,
      autoplay: true,

    };
    
    return (
        <div className="slider-container">
            <Slider {...settings}>
              <div className="slider-img"><img width="100%" src='./images/slider/bg1.jpg' alt="slide1"/></div>
              <div className="slider-img"><img width="100%" src='./images/slider/bg2.jpg' alt="slide2"/></div>
              <div className="slider-img"><img width="100%" src='./images/slider/bg3.jpg' alt="slide3"/></div>
            </Slider>
        </div>
    )
  }
}

export default SliderMain;