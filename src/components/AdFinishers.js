import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import './AdFinishers.css';


class AdFinishers extends Component {
	handleSubmit(values){
		console.log(values);
	}
  render() {
    return (
      <form onSubmit={this.handleSubmit} className="form">
        <div>
          <label className="formLabel" htmlFor="firstName">First Name</label>
          <Field className="formField" name="firstName" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="middleName">Middle Name</label>
          <Field className="formField" name="middleName" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="lastName">Last Name</label>
          <Field className="formField" name="lastName" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="photo">Photo</label>
          <Field className="formField" name="photo" component="input" type="file"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="about">About</label>
          <Field className="textareaField formField" name="about" component="textarea" rows="10" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="sport">Sport</label>
          <Field className="formField" name="sport" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="city">City</label>
          <Field className="formField" name="city" component="input" type="text"/>
        </div>
        <div>
          <label className="formLabel" htmlFor="age">Age</label>
          <Field className="formField" name="age" component="input" type="text"/>
        </div>
        <div>
        <label className="formLabel">Gender</label>
        <div>
          <Field className="selectField formField" name="gender" component="select">
            <option></option>
            <option value="0">Male</option>
            <option value="1">Female</option>
          </Field>
        </div>
      </div>
        
        <button className="btn btn-success" type="submit">Добавить</button>
      </form>
    )
  }
}

AdFinishers = reduxForm({
  form: 'finisher' // a unique name for this form
})(AdFinishers);

export default AdFinishers;