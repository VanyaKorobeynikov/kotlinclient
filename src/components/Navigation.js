import React from 'react'
import { Link } from 'react-router'
import Nav from 'react-bootstrap/lib/Nav'
import Navbar from 'react-bootstrap/lib/Navbar'
import NavItem  from 'react-bootstrap/lib/NavItem'
import NavDropdown  from 'react-bootstrap/lib/NavDropdown'
import MenuItem from 'react-bootstrap/lib/MenuItem'
import './Navigation.css'
import { LinkContainer } from 'react-router-bootstrap'


const Navigation = () => (
  <Navbar inverse collapseOnSelect>
    <Navbar.Header>
      <Navbar.Brand><Link to="/" className="navbar-brand logo" href="index.html"><img src="/images/kotlinrace.png" alt="logo" width="100%" /></Link></Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight>
        <LinkContainer to="/finishers">
          <NavItem className="menuItem">Финишеры</NavItem>
        </LinkContainer>
        <LinkContainer to="/photos">
          <NavItem className="menuItem">Фото</NavItem>
        </LinkContainer>
        <LinkContainer to="/reports">
          <NavItem className="menuItem">Отчеты</NavItem>
        </LinkContainer>
        <LinkContainer to="/culture">
          <NavItem className="menuItem">Культура</NavItem>
        </LinkContainer>
        <LinkContainer to="/geo">
          <NavItem className="menuItem">География</NavItem>
        </LinkContainer>
        <LinkContainer to="/Partners">
          <NavItem className="menuItem">Партнеры</NavItem>
        </LinkContainer>
        <NavDropdown className="menuItem dropItem" title="О заплыве" id="nav-dropdown">
          <LinkContainer to="/about"><MenuItem>О заплыве</MenuItem></LinkContainer>
          <LinkContainer to="/history"><MenuItem>История</MenuItem></LinkContainer>
          <LinkContainer to="/rules"><MenuItem>Правила</MenuItem></LinkContainer>
          <LinkContainer to="/authors"><MenuItem>Об организаторах</MenuItem></LinkContainer>
        </NavDropdown>
        <LinkContainer to="/registration" active>
          <NavItem className="menuItem">Зарегистрироваться</NavItem>
        </LinkContainer>

      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

export default Navigation
