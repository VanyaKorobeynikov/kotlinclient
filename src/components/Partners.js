import React, { Component } from 'react';
import './Photos.css';
import Gallery from './Gallery';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as partnersActions from '../actions/Partners';
import Partner from './Partner';

class Partners extends Component{
  componentWillMount() {
    this.props.partnersActions.getPartners();
  }
  
  render() {
    const { partners } = this.props
    
    return (
      <div className="container">
        <div className="center">
             <h2>Партнеры</h2>
        </div>
        <div className="flex-container">
          {this.props.fetching ? 
            <div className=".loader"></div>
            : 
            partners.map((item, index) => (<Partner partner={item} key={index}/>) )
          }
        </div>        
      </div>
      
    )
  }
}

function mapStateToProps(state){
  return {
    partners: state.partners.partners,
    fetching: state.partners.fetching,
  }
}

function mapDispatchToProps(dispatch){
  return {
    partnersActions: bindActionCreators(partnersActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Partners);