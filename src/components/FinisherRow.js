import React, { Component } from 'react';

const FinisherRow = (props) => {
  const { item, start } = props
  return (
    <div className="divTableRow">
      <div className="divTableCell">{item.name}</div>
      <div className="divTableCell">{item.city}</div>
      <div className="divTableCell">{item.country}</div>
      <div className="divTableCell">{item.age}</div>
      <div className="divTableCell">{item.sex}</div>
      <div className="divTableCell">{start.wetsuite ? 'yes':'no'}</div>
      <div className="divTableCell">{start.date}</div>
      <div className="divTableCell">{start.time}</div>
      <div className="divTableCell">{start.type}</div>
    </div>)
}

export default FinisherRow;