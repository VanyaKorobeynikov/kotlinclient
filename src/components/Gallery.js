import React, { Component } from 'react';
import Lightbox from 'react-images';
import './Photos.css';

class Gallery extends Component{

  constructor (props) {
    super(props);
    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
      images: this.props.photos,
      backgroundSize: 'cover',
      backgroundPosition: '0% 35%',
    };

    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    this.gotoImage = this.gotoImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
  }
  openLightbox (index, event) {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox () {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious () {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext () {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  gotoImage (index) {
    this.setState({
      currentImage: index,
    });
  }
  handleClickImage () {
    if (this.state.currentImage === this.state.images.length - 1) return;

    this.gotoNext();
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      images: nextProps.photos,
    })
  }
  renderGallery () {
    const images = this.state.images;
    const gallery = images.map((obj, i) => {
      return (
        <a
          href={obj.src}
          className="photo"
          key={i}
          onClick={(e) => this.openLightbox(i, e)}
          style={{
            background: `url(${obj.src})`,
          }}
        >
        <div className="photoCaption">
        {obj.caption}       
        </div></a>
      );
    });

    return (
      <div className="flex-container">
        {gallery}
      </div>
    );
  }


  render() {
    return (
      <div className="container">
        {this.renderGallery()}
        <Lightbox
          currentImage={this.state.currentImage}
          images={this.state.images}
          isOpen={this.state.lightboxIsOpen}
          onClickImage={this.handleClickImage}
          onClickPrev={this.gotoPrevious}
          onClickNext={this.gotoNext}
          onClose={this.closeLightbox}
        />
      </div>
      
    )
  }
}

export default Gallery;