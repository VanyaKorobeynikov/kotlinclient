import React, { Component } from 'react';
import { Link } from 'react-router';

class PageNotFound extends Component{
  componentDidMount() {
    document.title = "KotlinRace - страница не найдена";
  }
  render() {
    return (
      <div className="container">
        <h2>Страница не найдена</h2>
        <h2>К сожалению страницы по этому адресу не существует. <Link to="/">Перейти на главную</Link></h2>
      </div>
    )
  }
}


export default PageNotFound;