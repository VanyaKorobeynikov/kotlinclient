import React, { Component } from 'react';
import './App.css';
import './css/bootstrap.min.css';
import './css/main.css';
import Navigation from './components/Navigation';
import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigation />
        <div>
            {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
