import { ADD_REQUEST_REQUEST, ADD_REQUEST_SUCCESS, LEAVE, GET_REQUEST_REQUEST, GET_REQUEST_SUCCESS, DELETE_REQUEST_REQUEST, DELETE_REQUEST_SUCCESS } from '../constants/Request';
import axios from 'axios';
import { initialize } from 'redux-form';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import cookie from 'react-cookie';
import { server } from '../confy.js'

axios.defaults.headers.common['Authorization'] = cookie.load('token');



function requestAddRequest(){
  return {
    type: ADD_REQUEST_REQUEST
  }
}

function addRequestSuccess(request){
  return {
    type: ADD_REQUEST_SUCCESS,
    request: request
  }
}
function requestGetRequests(){
  return {
    type: GET_REQUEST_REQUEST
  }
}

function receiveRequests(requests){
  return {
    type: GET_REQUEST_SUCCESS,
    requests: requests
  }
}
function requestDeleteRequest(){
  return {
    type: DELETE_REQUEST_REQUEST
  }
}
function requestDeleteRequest(){
  return {
    type: DELETE_REQUEST_REQUEST
  }
}

function deleteRequestSuccess(){
  return {
    type: DELETE_REQUEST_SUCCESS
  }
}
export function leave(){
  return {
    type: LEAVE
  }
}

export function getRequests() {
  return (dispatch) => {
    
    dispatch(requestGetRequests())

    return axios.get(server+'/api/requests')
      .then((response) => dispatch(receiveRequests(response.data)))
      .catch((error) => console.log(error));
    
  }
}

/*
export function getPhoto(id) {
  return (dispatch) => {
    
    dispatch(requestPhoto(id))

    return axios.get(server+'/api/photo/'+id)
      .then((response) => dispatch(receivePhoto(response.data)))
      .then((response) => dispatch(initialize('editPhoto',response.photo)))
      .catch((error) => console.log(error));
    
  }
}

export function deletePhoto(id) {
  return (dispatch) => {
    
    dispatch(requestDelete())

    return axios.get(server+'/api/photo/'+id+'/delete')
      .then((response) => dispatch(completeDelete(response.data)))
      .then(() => dispatch(getPhotos(1)))
      .catch((error) => console.log(error));
    
  }
}

export function editPhoto(photo) {
  return (dispatch) => {
    dispatch(requestEditPhoto(photo))

    return axios.post(server+'/api/photo/edit', photo)
      .then((response) => {
        if(response.status === 200) return dispatch(editPhotoSuccess(response.data))
      })
      
      .catch((error) => console.log(error));
  }
}
*/


export function addRequest(request) {
  return (dispatch) => {
    dispatch(requestAddRequest())
    return axios.post(server+'/api/request/add', request)
      .then((response) => {
        
        if(response.status === 200) return dispatch(addRequestSuccess(response.data))
      })
      .catch((error) => console.log(error));
  }
}


export function deleteRequest(id) {
  return (dispatch) => {
    dispatch(requestDeleteRequest())
    return axios.get(server+'/api/request/'+id+'/delete')
      .then((response) => {
        
        if(response.status === 200) return dispatch(deleteRequestSuccess(response.data))
      })
      .then(() => dispatch(getRequests()))
      .catch((error) => console.log(error));
  }
}

