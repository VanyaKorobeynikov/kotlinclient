import { 
  GET_PHOTOS_REQUEST, 
  GET_PHOTOS_SUCCESS, 
  GET_PHOTO_REQUEST, 
  GET_PHOTO_SUCCESS, 
  ADD_PHOTO_REQUEST, 
  ADD_PHOTO_SUCCESS, 
  EDIT_PHOTO_REQUEST, 
  EDIT_PHOTO_SUCCESS, 
  DELETE_PHOTO_REQUEST, 
  DELETE_PHOTO_SUCCESS,
  FILTER_PHOTO,
} from '../constants/Photos';
import axios from 'axios';
import { initialize } from 'redux-form';
import { browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import cookie from 'react-cookie';
import { server } from '../confy.js'

axios.defaults.headers.common['Authorization'] = cookie.load('token');


function requestPhotos(year){
  return {
    type: GET_PHOTOS_REQUEST,
    year: year
  }
}

function receivePhotos(photos){
  return {
    type: GET_PHOTOS_SUCCESS,
    photos: photos
  }
}

function requestPhoto(id){
  return {
    type: GET_PHOTO_REQUEST,
    id: id
  }
}

function receivePhoto(photo){
  return {
    type: GET_PHOTO_SUCCESS,
    photo: photo
  }
}

function requestAddPhoto(){
  return {
    type: ADD_PHOTO_REQUEST
  }
}

function addPhotoSuccess(photo){
  return {
    type: ADD_PHOTO_SUCCESS,
    photo: photo
  }
}

function requestEditPhoto(){
  return {
    type: EDIT_PHOTO_REQUEST
  }
}

function editPhotoSuccess(photo){
  return {
    type: EDIT_PHOTO_SUCCESS,
    photo: photo
  }
}

function requestDelete(){
  return {
    type: DELETE_PHOTO_REQUEST
  }
}

function completeDelete(){
  return {
    type: DELETE_PHOTO_SUCCESS
  }
}


export function filterPhoto(year) {
  return (dispatch) => {
    dispatch({
      type: FILTER_PHOTO,
      year
    })
  }
}

export function getPhotos(year) {
  return (dispatch) => {

    dispatch(requestPhotos(year))

    return axios.get(server+'/api/photos/'+year)
      .then((response) => dispatch(receivePhotos(response.data)))
      .catch((error) => console.log(error));
    
  }
}

export function getPhoto(id) {
  return (dispatch) => {
    
    dispatch(requestPhoto(id))

    return axios.get(server+'/api/photo/'+id)
      .then((response) => dispatch(receivePhoto(response.data)))
      .then((response) => dispatch(initialize('editPhoto',response.photo)))
      .catch((error) => console.log(error));
    
  }
}

export function deletePhoto(id) {
  return (dispatch) => {
    
    dispatch(requestDelete())

    return axios.get(server+'/api/photo/'+id+'/delete')
      .then((response) => dispatch(completeDelete(response.data)))
      .then(() => dispatch(getPhotos(1)))
      .catch((error) => console.log(error));
    
  }
}

export function editPhoto(photo) {
  return (dispatch) => {
    dispatch(requestEditPhoto(photo))

    return axios.post(server+'/api/photo/edit', photo)
      .then((response) => {
        if(response.status === 200) return dispatch(editPhotoSuccess(response.data))
      })
      
      .catch((error) => console.log(error));
  }
}



export function addPhoto(photo) {
  return (dispatch) => {
    dispatch(requestAddPhoto(photo))
    let form = new FormData();
    form.append('caption', photo.caption);
    form.append('finisher', photo.finisher);
    form.append('start', photo.start)
    form.append('year', photo.year);
    form.append('photo', photo.file[0]);
    

    return axios.post(server+'/api/photo/add', form, { headers: { 'content-type': 'multipart/form-data' }})
      .then((response) => {
        
        if(response.status === 200) return dispatch(addPhotoSuccess(response.data))
      })
      .catch((error) => console.log(error));
  }
}

