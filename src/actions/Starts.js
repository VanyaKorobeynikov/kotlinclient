import { ADD_START_REQUEST, ADD_START_SUCCESS, GET_STARTS_REQUEST, GET_STARTS_SUCCESS, GET_START_REQUEST, GET_START_SUCCESS, DELETE_START_REQUEST, DELETE_START_SUCCESS, EDIT_START_SUCCESS } from '../constants/Starts';
import axios from 'axios';
import { initialize } from 'redux-form';
import cookie from 'react-cookie';
import { server } from '../confy.js'

axios.defaults.headers.common['Authorization'] = cookie.load('token');

function addStartRequest(){
  return {
    type: ADD_START_REQUEST,
  }
}

function addStartSuccess(start){
  
  return {
    type: ADD_START_SUCCESS,
    start
    
  }
}

function editStartSuccess(start){
  
  return {
    type: EDIT_START_SUCCESS,
    start
    
  }
}

function requestStarts(){
  return {
    type: GET_STARTS_REQUEST
  }
}

function receiveStarts(starts){
  return {
    type: GET_STARTS_SUCCESS,
    starts: starts
  }
}
function requestStart(){
  return {
    type: GET_START_REQUEST
  }
}

function receiveStart(start){
  return {
    type: GET_START_SUCCESS,
    start: start
  }
}

function requestDelete(){
  return {
    type: DELETE_START_REQUEST
  }
}

function completeDelete(){
  return {
    type: DELETE_START_SUCCESS
  }
}

export function getStarts(type) {
  return (dispatch) => {
    
    dispatch(requestStarts())

    return axios.get(server+'/api/starts/'+type)
      .then((response) => dispatch(receiveStarts(response.data)))
      .catch((error) => console.log(error));
    
  }
}

export function getStart(id) {
  return (dispatch) => {
    
    dispatch(requestStart())

    return axios.get(server+'/api/start/'+id)
      .then((response) => dispatch(receiveStart(response.data)))
      .then((response) => {
        const date = new Date(response.start.date);
        const strDate = date.getFullYear() +'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
        response.start.date = strDate;
        dispatch(initialize('start', response.start))
      })
      .catch((error) => console.log(error));
    
  }
}

export function deleteStart(id) {
  return (dispatch) => {
    
    dispatch(requestDelete())

    return axios.get(server+'/api/start/'+id+'/delete')
      .then((response) => dispatch(completeDelete()))
      .then(() => dispatch(getStarts()))
      .catch((error) => console.log(error));
    
  }
}

export function addStart(start) {
  
  return (dispatch) => {
    
    dispatch(addStartRequest())

    if(start._id){
      return axios.post(server+'/api/start/edit', start)
        .then((response) => {
          if(response.status === 200) return dispatch(editStartSuccess(response.data))
        })
        .then(() => dispatch(getStarts()))
        .catch((error) => console.log(error));
    }else{
      return axios.post(server+'/api/start/add', start)
        .then((response) => {
          if(response.status === 200) return dispatch(addStartSuccess(response.data))
        })
        .then(() => dispatch(getStarts()))
        .catch((error) => console.log(error));
    }
  }
}

