import { 
	GET_FINISHERS_REQUEST, 
	GET_FINISHERS_SUCCESS, 
	ADD_FINISHER_REQUEST, 
	ADD_FINISHER_SUCCESS, 
	GET_FINISHER_REQUEST, 
	GET_FINISHER_SUCCESS, 
	EDIT_FINISHER_SUCCESS,
	DELETE_FINISHER_REQUEST, 
	DELETE_FINISHER_SUCCESS,
	LEAVE,
} from '../constants/Finishers'
import axios from 'axios'
import { initialize } from 'redux-form'
import cookie from 'react-cookie'
import { server } from '../confy.js'

axios.defaults.headers.common['Authorization'] = cookie.load('token')

const receiveFinishers = (finishers) => ({
	type: GET_FINISHERS_SUCCESS,
	finishers: finishers,
})

const receiveFinisher = (finisher) => ({
		type: GET_FINISHER_SUCCESS,
		finisher: finisher,
})

const addFinisherSuccess = (finisher) => ({
		type: ADD_FINISHER_SUCCESS,
		finisher
})

const editFinisherSuccess = (finisher) => ({
		type: EDIT_FINISHER_SUCCESS,
		finisher
})

export const leave = () => ({ type: LEAVE })

export const getFinishers = () => ((dispatch) => {
		dispatch({ type: GET_FINISHERS_REQUEST })
		return axios.get(`${server}/api/finishers`)
			.then((response) => dispatch(receiveFinishers(response.data)))
			.catch((error) => console.log(error))
	}
)

export const deleteFinisher = (id) => ((dispatch) => {
		dispatch({ type: DELETE_FINISHER_REQUEST })
		return axios.get(`${server}/api/finisher/${id}/delete`)
			.then(() => dispatch({ type: DELETE_FINISHER_SUCCESS }))
			.then(() => dispatch(getFinishers()))
			.catch((error) => console.log(error))
		
	}
)

export const getFinisher = (id) => ((dispatch) => {
		dispatch({ type: GET_FINISHER_REQUEST })
		return axios.get(`${server}/api/finisher/${id}`)
			.then(({ data }) => dispatch(receiveFinisher(data)))
			.then(({ finisher }) => dispatch(initialize('finisher', finisher)))
			.catch((error) => console.log(error))
	}
)

export const addFinisher = (finisher) => ((dispatch) => {
		dispatch({ type: ADD_FINISHER_REQUEST })

		let finisherFormData = new FormData()

		Object
			.keys(finisher)
			.map((key) => {
				if (key === 'photo') {
					finisherFormData.append(key, finisher[key][0])
				}
				else {
					finisherFormData.append(key, finisher[key])
				}
			})

		const options = {
			headers: { 
				'content-type' : 'multipart/form-data',
			},
		}

		if (!finisher._id) {
			return axios.post(
				`${server}/api/finisher/add`, 
				finisherFormData, 
				options
			)
				.then(({ status, data }) => {
					if(status === 200) 
						return dispatch(addFinisherSuccess(data))
				})
				.then(() => (dispatch(getFinishers())))
				.catch((error) => console.log(error))
		}
		else {
			return axios.post(
				 `${server}/api/finisher/edit`, 
				 finisherFormData, 
				 options,
			)
				.then(({ status, data }) => {
					if (status === 200) 
						return dispatch(editFinisherSuccess(data))
				})
				.then(() => (dispatch(getFinishers())))
				.catch((error) => console.log(error))
		}
	}
)

