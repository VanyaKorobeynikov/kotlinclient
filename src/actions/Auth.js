import axios from 'axios'
import { browserHistory } from 'react-router'
import cookie from 'react-cookie'
import qs from 'qs'
import { AUTH_USER,  
         AUTH_ERROR,
         UNAUTH_USER,
         PROTECTED_TEST 
        } from '../constants/Auth'
import { server } from '../confy.js'


const API_URL = server

export function errorHandler(dispatch, error, type) {  
  let errorMessage = ''

  if(error.data.error) {
    errorMessage = error.data.error
  } else if(error.data){
    errorMessage = error.data
  } else {
    errorMessage = error
  }

  if(error.status === 401) {
    dispatch({
      type: type,
      payload: 'You are not authorized to do this. Please login and try again.'
    })
    logoutUser()
  } else {
    dispatch({
      type: type,
      payload: errorMessage
    })
  }
}

export function loginUser({ email, password }) {  
var params = new URLSearchParams()
params.append('email', email)
params.append('password', password)

  return function(dispatch) {
    axios.post(`${API_URL}/api/login`, qs.stringify({email,password}))
    .then(response => {
      cookie.save('token', response.data.token, { path: '/' })
      dispatch({ type: AUTH_USER })
      window.location.href = '/admin'
    })
    .catch((error) => {
      errorHandler(dispatch, error.response, AUTH_ERROR)
    })
  }
}

export function registerUser({ email, password }) {  
  return function(dispatch) {
    axios.post(`${API_URL}/api/register`, qs.stringify({ email, password }))
      .then(response => {
        cookie.save('token', response.data.token, { path: '/' })
        dispatch({ type: AUTH_USER })
        window.location.href = '/'
      })
      .catch((error) => {
        errorHandler(dispatch, error.response, AUTH_ERROR)
      })
  }
}

export function logoutUser() {  
  return function (dispatch) {
    dispatch({ type: UNAUTH_USER })
    cookie.remove('token', { path: '/' })

    window.location.href = '/login'
  }
}

export function protectedTest() {  
  return function(dispatch) {
    axios.get(`${API_URL}/api/protected`, {
      headers: { 'Authorization': cookie.load('token') }
    })
    .then(response => {
      dispatch({
        type: PROTECTED_TEST,
        payload: response.data.content
      })
    })
    .catch((error) => {
      errorHandler(dispatch, error.response, AUTH_ERROR)
    })
  }
}