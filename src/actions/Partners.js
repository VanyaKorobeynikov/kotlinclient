import { 
	GET_PARTNERS_REQUEST, 
	GET_PARTNERS_SUCCESS, 
	GET_PARTNER_REQUEST, 
	GET_PARTNER_SUCCESS, 
	ADD_PARTNER_REQUEST, 
	ADD_PARTNER_SUCCESS, 
	EDIT_PARTNER_REQUEST, 
	EDIT_PARTNER_SUCCESS, 
	DELETE_PARTNER_REQUEST, 
	DELETE_PARTNER_SUCCESS, 
} from '../constants/Partners'
import axios from 'axios'
import { initialize } from 'redux-form'
import { browserHistory } from 'react-router'
import { push } from 'react-router-redux'
import cookie from 'react-cookie'
import { server } from '../confy.js'

axios.defaults.headers.common['Authorization'] = cookie.load('token')

const receivePartners = (partners) => ({
		type: GET_PARTNERS_SUCCESS,
		partners
})

const requestPartner = (id) => ({
		type: GET_PARTNER_REQUEST,
		id
})

const receivePartner = (partner) => ({
		type: GET_PARTNER_SUCCESS,
		partner
})

const addPartnerSuccess = (partner) => ({
		type: ADD_PARTNER_SUCCESS,
		partner
})

const editPartnerSuccess = (partner) => ({
		type: EDIT_PARTNER_SUCCESS,
		partner
})

export const getPartners = () => ((dispatch) => {
		dispatch({ type: GET_PARTNERS_REQUEST })
		return axios.get(`${server}/api/partners`)
			.then(({ data }) => dispatch(receivePartners(data)))
			.catch((error) => console.log(error))
	}
)

export const getPartner = (id) => ((dispatch) => {
		dispatch(requestPartner(id))
		return axios.get(`${server}/api/partner/${id}`)
			.then(({ data }) => dispatch(receivePartner(data)))
			.then(({ partner }) => dispatch(initialize('editPartner', partner)))
			.catch((error) => console.log(error))
	}
)

export const deletePartner = (id) => ((dispatch) => {
		dispatch({ type: DELETE_PARTNER_REQUEST })
		return axios.get(`${server}/api/partner/${id}/delete`)
			.then(() => dispatch({ type: DELETE_PARTNER_SUCCESS }))
			.then(() => dispatch(getPartners(1)))
			.catch((error) => console.log(error))
	}
)

export const editPartner = (partner) => ((dispatch) => {
		dispatch({ type: EDIT_PARTNER_REQUEST })
		return axios.post(`${server}/api/partner/edit`, partner)
			.then(({ status, data }) => {
				if(status === 200) 
					return dispatch(editPartnerSuccess(data))
			})
			.catch((error) => console.log(error))
	}
)

export const addPartner = (partner) => ((dispatch) => {
		dispatch({ type: ADD_PARTNER_REQUEST })
		let form = new FormData()
		form.append('title', partner.title)
		form.append('annotation', partner.annotation)
		form.append('link', partner.link)
		form.append('partnerPhoto', partner.file[0])
		
		return axios.post(
			`${server}/api/partner/add`, 
			form, 
			{ 
				headers: 
				{ 
					'content-type': 'multipart/form-data',
				}
			})
			.then(({ status, data }) => {
				if(status === 200) 
					return dispatch(addPartnerSuccess(data))
			})
			.catch((error) => console.log(error))
	}
)

