import { 
  GET_PHOTOS_REQUEST, 
  GET_PHOTOS_SUCCESS, 
  GET_PHOTO_REQUEST, 
  GET_PHOTO_SUCCESS, 
  ADD_PHOTO_REQUEST, 
  ADD_PHOTO_SUCCESS, 
  EDIT_PHOTO_REQUEST, 
  EDIT_PHOTO_SUCCESS,
  FILTER_PHOTO,
} from '../constants/Photos';


const initialState = {
  photos: [],
  filteredPhotos: [],
  fetching: false,
  year: 0,
  photo: {}
}

export default function photos(state = initialState, action){
 switch(action.type){
    case GET_PHOTOS_REQUEST:
      return { ...state, year: action.year, fetching: true }
    case GET_PHOTOS_SUCCESS:
      return { 
        ...state, 
        photos: action.photos, 
        fetching: false,
        filteredPhotos: action.photos,
      }
    case GET_PHOTO_REQUEST:
      return { ...state, id: action.id, fetching: true }
    case GET_PHOTO_SUCCESS:
      return { ...state, photo: action.photo, fetching: false }
    case ADD_PHOTO_REQUEST:
      return { ...state, fetching: true }
    case ADD_PHOTO_SUCCESS:
      return { ...state, photo: action.photo, fetching: false }
    case EDIT_PHOTO_REQUEST:
      return { ...state, fetching: true }
    case EDIT_PHOTO_SUCCESS:
      return { ...state, photo: action.photo, fetching: false }
    case FILTER_PHOTO:
      return { 
        ...state, 
        year: action.year, 
        filteredPhotos: state.photos.filter((item) => (action.year === 0 || item.year === action.year)),
      }
    default:
      return state;
  } 
  
}