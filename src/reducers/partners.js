import { GET_PARTNERS_REQUEST, GET_PARTNERS_SUCCESS, GET_PARTNER_REQUEST, GET_PARTNER_SUCCESS, ADD_PARTNER_REQUEST, ADD_PARTNER_SUCCESS, EDIT_PARTNER_REQUEST, EDIT_PARTNER_SUCCESS } from '../constants/Partners';


const initialState = {
  partners: [],
  fetching: false,
  partner: {}
}

export default function partners(state = initialState, action){
 switch(action.type){
    case GET_PARTNERS_REQUEST:
      return { ...state, year: action.year, fetching: true }
    case GET_PARTNERS_SUCCESS:
      return { ...state, partners: action.partners, fetching: false }
    case GET_PARTNER_REQUEST:
      return { ...state, id: action.id, fetching: true }
    case GET_PARTNER_SUCCESS:
      return { ...state, partner: action.partner, fetching: false }
    case ADD_PARTNER_REQUEST:
      return { ...state, fetching: true }
    case ADD_PARTNER_SUCCESS:
      return { ...state, partner: action.partner, fetching: false }
    case EDIT_PARTNER_REQUEST:
      return { ...state, fetching: true }
    case EDIT_PARTNER_SUCCESS:
      return { ...state, partner: action.partner, fetching: false }
    default:
      return state;
  } 
  
}