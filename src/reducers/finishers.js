import { 
	GET_FINISHERS_REQUEST, 
	GET_FINISHERS_SUCCESS, 
	ADD_FINISHER_REQUEST, 
	ADD_FINISHER_SUCCESS, 
	GET_FINISHER_REQUEST, 
	GET_FINISHER_SUCCESS,  
	EDIT_FINISHER_REQUEST, 
	EDIT_FINISHER_SUCCESS, 
	LEAVE,
	DELETE_FINISHER_REQUEST, 
	DELETE_FINISHER_SUCCESS
} from '../constants/Finishers'


const initialState = {
	finishers: [],
	markers: [],
	fetching: false,
	added: false,
	finisher: {},
}

export default function finishers(state = initialState, action) {
 switch (action.type) {
		case GET_FINISHERS_REQUEST:
			return { ...state, fetching: true }
		case GET_FINISHERS_SUCCESS:
			return { ...state, finishers: action.finishers, fetching: false }
		case ADD_FINISHER_REQUEST: 
			return {...state, added: false} 
		case ADD_FINISHER_SUCCESS: 
			return {...state, finishers: state.finishers.push(action.finisher), added: true} 
		case GET_FINISHER_REQUEST: 
			return {...state, fetching: true} 
		case GET_FINISHER_SUCCESS: 
			return {...state, finisher: action.finisher, fetching: false} 
		case EDIT_FINISHER_SUCCESS: 
			return {...state, finisher: action.finisher, fetching: false} 
		case LEAVE: 
			return {...state, added: false} 
		default:
			return state
	} 
}