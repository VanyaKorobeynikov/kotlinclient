import { ADD_START_REQUEST, ADD_START_SUCCESS, GET_STARTS_REQUEST, GET_STARTS_SUCCESS, GET_START_REQUEST, GET_START_SUCCESS, DELETE_START_REQUEST, DELETE_START_SUCCESS, EDIT_START_SUCCESS } from '../constants/Starts';

const initialState = {
  starts: [],
  start: {},
  fetching: false,
  deleting: false
}

export default function starts(state = initialState, action){
 switch(action.type){
    case GET_STARTS_REQUEST:
      return { ...state, fetching: true }
    case GET_STARTS_SUCCESS:
      return { ...state, starts: action.starts, fetching: false }
    case GET_START_REQUEST:
      return { ...state, fetching: true }
    case GET_START_SUCCESS:
      return { ...state, start: action.start, fetching: false }
    case ADD_START_REQUEST: 
      return {...state, added: false} 
    case ADD_START_SUCCESS: 
      return {...state, added: true} 
    case DELETE_START_REQUEST: 
      return {...state, deleting: true} 
    case DELETE_START_SUCCESS: 
      return {...state, deleting: false}
    case EDIT_START_SUCCESS: 
      return {...state, start: action.start, fetching: false} 
    default:
      return state;
  } 
  
}