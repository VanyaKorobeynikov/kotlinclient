import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import finishers from './finishers';
import starts from './starts';
import photos from './photos';
import request from './request';
import auth from './auth';
import partners from './partners';


export default combineReducers({
  routing: routerReducer,
  starts,
  finishers, 
  photos,
  request,
  auth,
  partners,
  form: formReducer
  
})
