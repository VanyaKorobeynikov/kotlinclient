import { ADD_REQUEST_REQUEST, ADD_REQUEST_SUCCESS, LEAVE, GET_REQUEST_REQUEST, GET_REQUEST_SUCCESS, DELETE_REQUEST_REQUEST, DELETE_REQUEST_SUCCESS } from '../constants/Request';

const initialState = {
  requests: [],
  request: {},
  fetching: false,
  added: false
}

export default function request(state = initialState, action){
 switch(action.type){
    case ADD_REQUEST_REQUEST:
      return { ...state, fetching: true}
    case ADD_REQUEST_SUCCESS:
      return { ...state, fetching: false, request: action.request, added: true }
    case LEAVE:
      return { ...state, added: false }
    case GET_REQUEST_REQUEST:
      return { ...state, fetching: true}
    case GET_REQUEST_SUCCESS:
      return { ...state, fetching: false, requests: action.requests }
    default:
      return state;
  } 
  
}
